import java.io.*;
import java.util.*;
import java.text.*;
import static java.lang.System.*;
import static java.lang.Integer.*;
import static java.lang.Double.*;
import static java.lang.Character.*;
import static java.util.Collections.*;
import static java.lang.Math.*;
import static java.util.Arrays.*;

public class problem3
{
	public void run()throws Exception
	{
		Scanner file=new Scanner(new File("problem3.dat"));
		int times = file.nextInt();
		for(int asdf = 0; asdf < times; asdf++)
		{
			double sum = 0.0;
			int types = file.nextInt();
			file.nextLine();
			String name = file.nextLine();
			if(types > 0 ) {
			ArrayList<Double> costs = new ArrayList<Double>();
			ArrayList<Integer> wants = new ArrayList<Integer>();
			for(int k = 0; k < types; k++){wants.add(file.nextInt());}
			for(int j = 0; j < types; j++){costs.add(file.nextDouble());}
			for(int  m = 0; m < types; m++)
			{
				if(wants.get(m) >=0)
				{
					sum += (costs.get(m)*wants.get(m));
				}
				
			}
			System.out.println("It will cost Sammy Klaws $" + sum + " to make " + name + " happy this Christmas!");}
			else System.out.println("It will be free for Sammy Klaws to make "+ name + " happy this Christmas!");
		}
		
	}

	public static void main(String[] args)throws Exception
	{
		problem3 x = new problem3();
		x.run();
	}
}