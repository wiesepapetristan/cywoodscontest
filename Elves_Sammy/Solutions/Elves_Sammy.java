import java.io.*;
import java.util.*;
import java.text.*;
class Elves
{
	public static void main(String[] args) throws Exception {new Elves().run();}
	public void run() throws Exception
	{
		Scanner file = new Scanner(new File("Elves_Sammy/Judge/elves.dat"));
		while(file.hasNext()) {
			String name=file.next().trim();
			String[]date=file.next().trim().split("/");
			int design=file.nextInt(),format=file.nextInt();
			int daysoff=file.nextInt();
			if(name.equals("SammyKlaws")) {
				System.out.printf("E: %.6f%n%n",Math.E);
				continue;
			}
			GregorianCalendar gc=new GregorianCalendar(Integer.decode(date[2]),Integer.decode(date[0])-1,Integer.decode(date[1]));
			SimpleDateFormat sdf=new SimpleDateFormat("!@#$%^&*(");
			System.out.println("Happy Birthday "+name+"!");
			switch(design) {
			case 1:System.out.println("  ^\r\n" + 
					" /^\\\r\n" + 
					" /^\\\r\n" + 
					"/^^^\\\r\n" + 
					"/^^^\\");break;
			case 2:System.out.println("  !\r\n" + 
					"  $\r\n" + 
					" $$$\r\n" + 
					" $$$\r\n" + 
					"  $");break;
			case 3:System.out.println(" +=+\r\n" + 
					"-----\r\n" + 
					"|212|\r\n" + 
					"|212|\r\n" + 
					"-----");break;
			case 4:System.out.println("  *\r\n" + 
					" ***\r\n" + 
					"*****\r\n" + 
					" ***\r\n" + 
					"  *");break;
			}
			gc.add(gc.DATE, daysoff);
			Date d=gc.getTime();
			switch(format) {
			case 1:sdf=new SimpleDateFormat("EEEE MMMM d, yyyy");break;
			case 2:sdf=new SimpleDateFormat("MM/dd/yy");break;
			case 3:sdf=new SimpleDateFormat("MMM dd, yy");break;
			}
			System.out.println(sdf.format(d));
			System.out.println();
		}
		
	}
}
