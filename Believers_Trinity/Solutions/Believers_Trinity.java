 import java.io.*;
import java.util.*;
import java.text.*;
import static java.lang.System.*;
import static java.lang.Integer.*;
import static java.lang.Double.*;
import static java.lang.Character.*;
import static java.util.Collections.*;
import static java.lang.Math.*;
import static java.util.Arrays.*;


class Believers
{
	//instance variables go here

	public void run()throws Exception
	{
		Scanner file=new Scanner(new File("believers.dat"));
		int times = file.nextInt();
		file.nextLine();
		
		for(int i = 0;i<times;i++)
		{
			int santa = 0;
			int times2 = file.nextInt();
			
			file.nextLine();
			
			for(int x = 0;x<times2;x++)
			{
				String str = file.nextLine();
				String[] s = str.split(" ");
				
				boolean believe = (s[2].equals("true"));
				int age = Integer.parseInt(s[1]);
				
				if(believe)
					santa++;
				if(!believe && age<13)
					santa--;
				
			}
			
			System.out.println("Santa Score: "+ santa);
		}


	}

	

	public static void main(String[] args)throws Exception
	{
		Believers a=new Believers();
		a.run();
	}
}