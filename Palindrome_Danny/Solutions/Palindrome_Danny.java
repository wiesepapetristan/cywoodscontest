package CywoodsProblems;

import java.io.File;
import java.util.Scanner;

class Palindrome {
	// instance variables go here

	public void run() throws Exception {
		Scanner file = new Scanner(new File("palindrome.dat"));
		int loops = file.nextInt();
		int n = loops;
		file.nextLine();
		for (int loop = 0; loop < loops; loop++) 
		{
			String curr = file.nextLine();
			StringBuffer line = new StringBuffer(curr);
			if(line.toString().equals(line.reverse().toString()))
			{
				System.out.println( line);
				
				String t = "";
				for(int i =1;i<line.length()-1;i++)
				{
					t += " ";
				}
				for(int i = 1;i<curr.length()-1;i++)
				{
					System.out.println(line.substring( i,i+1) + t +  line.substring( i, i+1) );
					
				}
				System.out.println( line);
				System.out.println( );
			}
			else
			{
				System.out.println( "\"NOT A PALINDROME!\"");
			}
			
		}
	}

	public static void main(String[] args) throws Exception {
		Palindrome a = new Palindrome();
		a.run();
	}
}