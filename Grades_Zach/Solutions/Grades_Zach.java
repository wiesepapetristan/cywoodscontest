import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

class Grades
{
	public void run()throws Exception
	{
		Scanner file=new Scanner(new File("Grades_Zach/Judge/grades.dat"));
		String s="";
		while(file.hasNextLine()) s+=file.nextLine();
		String[] list=s.split("(<a id=\"courseName\">)|(<a id=\"average\">)");
		for(int i=1;i<list.length-1;i+=2) {
			list[i]=list[i].replaceAll("</a>|<span.+>", "").trim();
			list[i+1]=list[i+1].replaceAll("(<span.+>)|(</a>)|(<.+>)", "").trim();
			//System.out.println(Arrays.toString(list));
			int copy=(int)Math.round(Double.parseDouble(list[i+1]));
			s=(copy>=90)?"Sammy has a "+copy+" in "+list[i]+" so he gets a Great Gift.":(copy>=80)?
					"Sammy has a "+copy+" in "+list[i]+" so he gets a Good Gift.":(copy>=70)?
					"Sammy has a "+copy+" in "+list[i]+" so he gets a Mediocre Gift.":"Sammy has a "+copy+" in "+list[i]+" so he gets Coal.";
			System.out.println(s);		
		}
	}

	public static void main(String[] args)throws Exception
	{
		Grades a=new Grades();
		a.run();
	}
}

//Sammy has a 92 in Calc-BC/Hap so he gets a Great Gift.
//Sammy has a 96 in AP PHYSICS II so he gets a Great Gift.
//Sammy has a 81 in MACROECONOMICS so he gets a Good Gift.
//Sammy has a 69 in RSCH COMP SCI K so he gets Coal.
